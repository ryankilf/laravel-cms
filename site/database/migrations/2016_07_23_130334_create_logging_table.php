<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateLoggingTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('model_changes', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('session_id')->index()->nullable();
            $table->string('request_id')->index();
            $table->string('user_id')->index()->nullable();
            $table->string('user_name')->index()->nullable();
            $table->string('operation')->index();
            $table->string('route')->index()->nullable();
            $table->string('method')->index()->nullable();
            $table->string('model_id')->index();
            $table->string('model_class')->index();
            $table->longText('changes')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('model_changes');
    }
}

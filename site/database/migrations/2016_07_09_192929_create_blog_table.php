<?php

use App\Blueprint\AppBlueprint;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateBlogTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {

        $schema = DB::connection()->getSchemaBuilder();
        $schema->blueprintResolver(function($table, $callback) {
            return new AppBlueprint($table, $callback);
        });

        $schema->create('blog_posts', function (AppBlueprint $table) {
            $table->increments('id');
            $table->integer('user_id')->unsigned()->index();
            $table->integer('category_id')->unsigned()->index();
            $table->integer('image')->unsigned()->index();
            $table->main_page_content_item();
            $table->timestamps();
            $table->date('publish_date');
            $table->softDeletes();

            $table->engine = 'InnoDB';
        });


        $schema->create('blog_categories', function (AppBlueprint $table) {
            $table->increments('id');
            $table->main_page_content_item();
            $table->timestamps();
            $table->softDeletes();

            $table->engine = 'InnoDB';
        });

        $schema->create('blog_tags', function (AppBlueprint $table) {
            $table->increments('id');
            $table->integer('user_id')->unsigned();
            $table->main_page_content_item();
            $table->timestamps();
            $table->softDeletes();

            $table->engine = 'InnoDB';
        });



        $schema->create('blog_post_blog_tag', function (AppBlueprint $table) {
            $table->integer('blog_post_id')->index();
            $table->integer('blog_tag_id')->index();
            $table->integer('order')->index();
            $table->engine = 'InnoDB';
        });




    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}

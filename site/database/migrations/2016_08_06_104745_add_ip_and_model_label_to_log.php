<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddIpAndModelLabelToLog extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('model_changes', function (Blueprint $table) {
            $table->string('client_ip')->nullable()->index();
            $table->string('model_label')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('model_changes', function (Blueprint $table) {
            $table->dropColumn('client_ip');
            $table->dropColumn('model_label');
        });
    }
}

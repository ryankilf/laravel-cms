<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

use Illuminate\Support\Facades\DB;

class CreateSiteSearchTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('site_search_items', function ($table) {
            $table->bigIncrements('id');
            $table->char('page_title');
            $table->text('page_content');
            $table->char('class_name');
            $table->integer('page_id')->unsigned()->index();
            $table->char('full_path')->index();
            $table->unique(['page_id', 'class_name']);
            $table->timestamps();

        });

        DB::statement('ALTER TABLE site_search_items ADD FULLTEXT search(page_title, page_content)');
        DB::statement('ALTER TABLE site_search_items ADD FULLTEXT title_search(page_title)');

    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('site_search_items');
    }
}

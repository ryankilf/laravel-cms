<div class="panel">
    <div class="panel-header"><h2>Model Change - {{ $modelChange->model_label }}</h2></div>
    <div class="panel-body">
        <div class="col-md-3">
            <table class="table table-striped">
                <tr>
                    <th>Date Time:</th>
                    <td> {{ $modelChange->created_at->format('d/m/Y H:i:s') }}</td>
                </tr>
                <tr>
                    <th>Session Id:</th>
                    <td><a href="/admin/model_changes?session_id={{ $modelChange->session_id }}">{{ $modelChange->session_id }}</a> </td>
                </tr>
                <tr>
                    <th>Request Id:</th>
                    <td><a href="/admin/model_changes?request_id={{ $modelChange->request_id }}">{{ $modelChange->request_id }}</a> </td>
                </tr>
                <tr>
                    <th>Username:</th>
                    <td>{{ $modelChange->user_name }}</td>
                </tr>
                <tr>
                    <th>Route:</th>
                    <td>{{ $modelChange->route }}</td>
                </tr>
                <tr>
                    <th>Method:</th>
                    <td>{{ $modelChange->method }}</td>
                </tr>
                <tr>
                    <th>Client IP:</th>
                    <td>{{ $modelChange->client_ip }}</td>
                </tr>
                <tr>
                    <th>Model Label:</th>
                    <td>{{ $modelChange->model_label }}</td>
                </tr>
            </table>
        </div>
        <div class="col-md-9">
            <table class="table table-striped">
                @foreach($modelChange->getChangesAsArray() as $key => $change)
                    <tr>
                        <th>{{ $key }}:</th>
                        <td>{{ $change }}</td>
                    </tr>
                @endforeach
            </table>
        </div>

    </div>
</div>
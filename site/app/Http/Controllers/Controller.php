<?php

namespace App\Http\Controllers;

use App\SitePage;
use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Http\Request;
use Illuminate\Routing\Controller as BaseController;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Foundation\Auth\Access\AuthorizesRequests;
use Illuminate\Foundation\Auth\Access\AuthorizesResources;
use Illuminate\Support\Facades\Route;
use Symfony\Component\HttpFoundation\Response;

class Controller extends BaseController implements AdminSelectableController
{
    use AuthorizesRequests, AuthorizesResources, DispatchesJobs, ValidatesRequests;

    public static $HIDE_FROM_ADMINISTRATION_OPTIONS = true;

    /**
     * You must extend either getResponse or addRoutesForU
     * @param Request $request
     * @param SitePage $associatedPage
     * @return Response
     */
    public function getResponse(Request $request, SitePage $associatedPage) : Response
    {
        throw new \Exception('You must extend either getResponse or addRoutesForUrl, or this happens');
    }

    /**
     * It's anticipated that this will only be executed whenever the AppServiceProvider boots
     * @param String $url
     */
    public static function addRoutesForUrl(String $url)
    {
        Route::get($url,
            function (Request $request, SitePage $sitePage) {
                $controller = new SubpageController();
                return $controller->getResponse($request, $sitePage);
            }
        );
    }

    public function getCommonPageDetails(Request $request): array
    {
        //Homepage is different
        if ($request->url() != '') {
            $exact = false;
            $page = SitePage::findNearestPageToUrl($request->url(), $exact);

        }

        $navigation = $this->getNavigation();
        return [
            'page' => $page,
            'navigation' => $navigation
        ];
    }

    /**
     * Don't just hard code these.
     * Possible strategies
     * - get all root elements [easiest]
     * - get all root elements and their immediate children
     * - have a flag in the SitePage table "Use for navigation"
     * - have a separate navigation baum nested table [most flexibility]
     *   -- if you're doing this, you need to have a link from the navigation to the page itself
     *   -- or it's not going to be a particularly fun time whenever the page moves/gets deleted/whatever
     *   -- (make using a URL instead an option, but make it easier to link to the page itself)
     *   -- Have a maximum depth in the settings on the admin setup for it
     *
     */
    public function getNavigation() {
        return [];
    }

}

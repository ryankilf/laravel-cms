<?php

namespace App\Http\Controllers;


use App\SitePage;
use Illuminate\Http\Request;
use Symfony\Component\HttpFoundation\Response;

interface AdminSelectableController
{

    
    
    public function getResponse(Request $request, SitePage $associatedPage) : Response;

    /**
     * It's anticipated that this will only be executed whenever the AppServiceProvider boots
     * @param String $url
     */
    public static function addRoutesForUrl(String $url);
}
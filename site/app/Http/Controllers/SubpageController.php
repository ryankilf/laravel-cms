<?php
/**
 * Created by PhpStorm.
 * User: ryan
 * Date: 12/07/2016
 * Time: 12:24
 */

namespace App\Http\Controllers;


use App\SitePage;
use Illuminate\Http\Request;
use Symfony\Component\HttpFoundation\Response;

class SubpageController extends Controller
{
    public static $HIDE_FROM_ADMINISTRATION_OPTIONS = false;

    public function getResponse(Request $request, SitePage $sitePage) : Response
    {
        $pageDetails = $this->getCommonPageDetails($request);
        return response(view('pages.generic', $pageDetails));
    }

}
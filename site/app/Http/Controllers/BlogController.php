<?php
namespace App\Http\Controllers;


use App\Models\BlogPost;
use App\SitePage;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;
use Symfony\Component\HttpFoundation\Response;

class BlogController extends Controller
{
    public static $HIDE_FROM_ADMINISTRATION_OPTIONS = false;

    public static function addRoutesForUrl(String $url)
    {
        Route::group(['as' => 'blog::'], function () use ($url) {

            Route::get($url, ['as' => 'index',
                function (Request $request) {
                    $controller = new BlogController();
                    return $controller->getBlogIndexResponse($request);
                }
            ]);

            Route::get($url . '/categories', ['as' => 'categories.index',
                function () {
                    $controller = new BlogController();
                    return $controller->getBlogCategoriesResponse();
                }
            ]);

            Route::get($url . '/{category}', ['as' => 'category',
                function ($category) {
                    $controller = new BlogController();
                    return $controller->getBlogCategoryResponse($category);
                }
            ]);

            Route::get($url . '/tags', ['as' => 'tags.index',
                function () {
                    $controller = new BlogController();
                    return $controller->getBlogTagsResponse();
                }
            ]);

            Route::get($url . '/tags/{tag}', ['as' => 'tag',
                function ($tag) {
                    $controller = new BlogController();
                    return $controller->getBlogTagResponse($tag);
                }
            ]);

            Route::get($url . '/{category}/{blog}', [ 'as' => 'item',
                function ($category, $blog) {
                    $controller = new BlogController();
                    return $controller->getBlogItemResponse($category, $blog);
                }
            ]);
        });
    }

    private function getBlogIndexResponse(Request $request)
    {
        $pageDetails = $this->getCommonPageDetails($request);
        $pageDetails['blogPosts'] = BlogPost::publicPosts()->paginate(20);
        return response(view('pages.generic', $pageDetails));
    }

    private function getBlogCategoryResponse($category)
    {
        return response('blog category:' . $category);
    }

    private function getBlogCategoriesResponse()
    {
        return response('blog categories index');
    }

    private function getBlogItemResponse($category, $blog)
    {
        return response('blog category:' . $category . ' item:' . $blog);
    }

    private function getBlogTagsResponse($tag)
    {
        return response('blog category:' . $tag);
    }

    private function getBlogTagResponse($tag)
    {
        return response('blog category:' . $tag);
    }
}
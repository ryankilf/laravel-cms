<?php

namespace App\Http\Controllers;


use App\Models\Log\ModelChange;
use App\User;
use Illuminate\Support\Facades\DB;
use PDO;

class ChangeLogViewer extends Controller
{


    public function display($id)
    {

        try {
            $modelChange = ModelChange::where(['id' => $id])->firstOrFail();
        } catch (\Exception $e) {
            abort(404);
        }
        $params = [
            'modelChange' => $modelChange
        ];
        return view('admin.change_log.one', $params)->render();

    }

}
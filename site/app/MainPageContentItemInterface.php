<?php
/**
 * Created by PhpStorm.
 * User: ryan
 * Date: 19/08/2016
 * Time: 19:34
 */

namespace App;


use Illuminate\Database\Eloquent\Model;

interface MainPageContentItemInterface
{
    public function setSlugFromField(Model $model, string $slugFieldName, string $basedOnField);

    public function setSlugFromValue(Model $model, string $slugFieldName, string $slugValue);

    public function getFullPath(): String;

    public function shouldBeIncludedInSiteSearch():bool;
}
<?php
namespace App;

use App\Http\Controllers\AdminSelectableController;
use App\Models\Log\LoggableModelInterface;
use App\Models\Log\LoggableModelTrait;
use Illuminate\Database\Eloquent\Model;

class SitePage extends \Baum\Node implements LoggableModelInterface, MainPageContentItemInterface {

    use MainPageContentItem;

    protected $table = 'site_pages';
    protected $orderBy = 'lft';
    protected $orderDirection = 'ASC';
    private $fullPath;


    public function getTitleWithDepth() {
        $twd = trim(str_repeat("--", count($this->getAncestors())).' '.$this->title);
        return $twd;
    }

    public static function boot()
    {
        parent::boot();
        static::setupLoggingEvents();
        static::setupSiteSearchEvents();

        static::creating(function(Model $model)
        {
            /**
             * The functionality in here isn't called directly, so that you can take over the boot method in your own model
             * while still using this trait
             */
            $model->makeSlugOnSave($model);
        });
    }

    public function getControllerName(): String
    {
        return  $this->controller;
    }

    public function getController(): AdminSelectableController {
        $cName = $this->getControllerName();
        return new $cName();
    }

    public static function findNearestPageToUrl(string $url, bool &$exactMatch = false): SitePage {
        $sitePages = SitePage::get();
        $url = '/'.$url;

        foreach ($sitePages as $sitePage) {
            if(!isset($closestMatch)) {
                $closestMatch = $sitePage;
            }
            if (stristr($url, $sitePage->getFullPath())) {
                if (
                    !isset($closestMatch) || strlen($closestMatch->getFullPath()) < strlen($sitePage->getFullPath())
                ) {
                    $closestMatch = $sitePage;
                }
            }
        }

        if (isset($closestMatch) && $closestMatch->getFullPath() == $url) {
            $exactMatch = true;
        }
        return $closestMatch;

    }

    public function getFullPath(): String
    {
        if (!is_null($this->fullPath)) {
            return $this->fullPath;
        }

        $ancestors = $this->ancestorsAndSelf()->get();
        $path = '';
        foreach($ancestors as $ancestor) {
            $path .= '/'.$ancestor->slug;
        }
        $this->fullPath = $path;
        return $path;
    }


    /**
     * Any field names that you do not want to log, at all go in here
     * e.g. there's no point in logging the laravel updated_at timestamp, so it should go in here.
     *
     * @return array
     */
    public function getDoNotLogFieldNames(): array
    {
        return [];
    }

    /**
     * If you want to log that a field has changed, but NOT what that field value is, it should go in here.
     * @return array
     */
    public function getMaskedFieldNames(): array
    {
        return [];
    }
}
<?php

namespace App\Providers;

use App\SitePage;
use Illuminate\Support\ServiceProvider;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        $roots = SitePage::roots()->get();
        foreach($roots as $root) {
            $pages = $root->descendantsAndSelf()->get();
            foreach ($pages as $page) {
                $controllerName = $page->getControllerName();
                $controllerName::addRoutesForUrl($page->getFullPath());
            }
        }
    }

    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        //
    }
}

<?php
namespace App\Blueprint;

class AppBlueprint extends \Illuminate\Database\Schema\Blueprint {

    public function main_page_content_item() {

        $this->char('title', 255);
        $this->text('introduction');
        $this->text('main_content');
        $this->slug();
        $this->text('meta_description');
    }

    public function slug() {
        $this->char('slug', 255)->unique();
    }
}

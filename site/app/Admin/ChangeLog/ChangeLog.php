<?php
use SleepingOwl\Admin\Model\ModelConfiguration;

AdminSection::registerModel(\App\Models\Log\ModelChange::class, function (ModelConfiguration $model) {
    $model->setTitle('Model Changes')->enableAccessCheck();

    // Display
    $model->onDisplay(function () {
        $table = AdminDisplay::datatablesAsync();
            $table->setFilters([
                AdminDisplayFilter::field('request_id')->setOperator('begins_with')
            ])
            ->setHtmlAttribute('class', 'table-primary')
            ->setColumns([
                AdminColumn::link('id')->setLabel('Id'),
                AdminColumn::text('session_id')->setLabel('Session Id'),
                AdminColumn::text('request_id')->setLabel('Request Id'),
                AdminColumn::text('model_class')->setLabel('Model Class'),
                AdminColumn::text('user_name', 'Username'),
                AdminColumn::text('client_ip', 'Client IP'),
                AdminColumn::datetime('created_at')->setLabel('Date'),
                AdminColumn::custom('')->setCallback(function(\App\Models\Log\ModelChange $modelChange) {
                   return '<a href="/admin/change-log/'.$modelChange->id.'" class="btn btn-primary btn-xs btn-flat" data-toggle="tooltip" title="View">
		                <i class="fa fa-search"></i></a>';
                })

            ])->paginate(20);
        $filters = [
            null,
            AdminColumnFilter::text()->setPlaceholder('Session Id'),
            AdminColumnFilter::text()->setPlaceholder('Request Id'),
            AdminColumnFilter::text()->setPlaceholder('Model Class'),
            AdminColumnFilter::select()->setModel(new \App\User())->setDisplay('name'),
            AdminColumnFilter::text()->setPlaceholder('Client IP'),
        ];
        $table->setColumnFilters($filters);


        return $table;
    });
});

<?php

Route::get('', ['as' => 'admin.dashboard', function () {
	$content = 'Define your dashboard here.';
	return AdminSection::view($content, 'Dashboard');
}]);

Route::get('information', ['as' => 'admin.information', function () {
	$content = 'Define your information here.';
	return AdminSection::view($content, 'Information');
}]);

Route::get('change-log/{id}', ['as' => 'admin.change_log', function ($id) {
    $changeLogViewer = new \App\Http\Controllers\ChangeLogViewer();
    return AdminSection::view($changeLogViewer->display($id), 'Change Log');
}]);
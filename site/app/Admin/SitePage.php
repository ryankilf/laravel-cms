<?php

use SleepingOwl\Admin\Model\ModelConfiguration;

AdminSection::registerModel(\App\SitePage::class, function (ModelConfiguration $model) {
    //$model->setTitle('Pages')->enableAccessCheck();

    // Display
    $model->onDisplay(function () {
        return AdminDisplay::tree()->setValue('title');
    });

    $model->onCreateAndEdit(function() {

        $controllersRelativeDir = '/Http/Controllers';
        $classBasePath = app_path().$controllersRelativeDir;

        $classDirectoryIterator = new RecursiveIteratorIterator(
            new RecursiveDirectoryIterator($classBasePath, RecursiveDirectoryIterator::SKIP_DOTS),
            RecursiveIteratorIterator::SELF_FIRST,
            RecursiveIteratorIterator::CATCH_GET_CHILD // Ignore "Permission denied"
        );

        $controllers = [];
        foreach ($classDirectoryIterator as $path => $dir) {
            if ($dir->isFile()) {

                //get rid of /var/www or /vagrant/www or whatever.
                $relativeFilePath = substr($dir->getPathname(), strlen(app_path()));

                //Get rid of .php
                $relativeClassPath = substr($relativeFilePath, 0, strlen($relativeFilePath) - 4);

                //array_filter to get rid of the blank initial array element
                $nameSpacedClassName = 'App'.str_replace(DIRECTORY_SEPARATOR, '\\', $relativeClassPath);

                $reflection = new ReflectionClass($nameSpacedClassName);

                /**
                 * Only select a class which implements the Interface (this is important for our routing)
                 * && is not the interface
                 * && which does not have the static property for hiding it from this
                 */
                if(
                    $reflection->implementsInterface(\App\Http\Controllers\AdminSelectableController::class)
                    && !$reflection->isAbstract() //Implement the interface, but don't BE the interface
                    && !$reflection->getStaticPropertyValue('HIDE_FROM_ADMINISTRATION_OPTIONS', false)
                ) {
                    $displayClassName = substr($relativeClassPath, strlen($controllersRelativeDir) + 1);
                    $displayClassName = str_replace(DIRECTORY_SEPARATOR, ' - ', $displayClassName);
                    $re = '/(?<=[a-z])(?=[A-Z])/x';
                    $a = preg_split($re, $displayClassName);
                    $displayClassName = join($a, " " );
                    $controllers[$nameSpacedClassName] = $displayClassName;
                }
            }
        }
        
        $pagesCollection = App\SitePage::orderBy('lft', 'asc')->get();
        $pages = [];
        foreach($pagesCollection as $page) {
            $pages[$page->id] = $page->getTitleWithDepth();
        }

        return AdminForm::form()->setItems([
            AdminFormElement::select('parent_id', 'Parent Page')
                ->nullable()
                ->setOptions($pages),
            AdminFormElement::select('controller', 'Controller')
                ->setOptions($controllers)->required(),
            AdminFormElement::textarea('meta_description', 'Meta Description (leave blank if you would like to use the introduction instead)'),
            AdminFormElement::text('title', 'Title')->required(),
            AdminFormElement::textarea('introduction', 'Introduction'),
            AdminFormElement::ckeditor('main_content', 'Main Content')
        ]);
    });
    
});
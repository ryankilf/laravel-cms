<?php
use SleepingOwl\Admin\Model\ModelConfiguration;

AdminSection::registerModel(\App\Models\BlogCategory::class, function (ModelConfiguration $model) {
    //$model->setTitle('Pages')->enableAccessCheck();

    // Display
    $model->onDisplay(function () {

        return AdminDisplay::table()
            ->setHtmlAttribute('class', 'table-primary')
            ->setColumns([
                AdminColumn::link('id')->setLabel('Id'),
                AdminColumn::email('title')->setLabel('Title')
            ])->paginate(20);
    });

    $model->onCreateAndEdit(function() {
        return AdminForm::form()->setItems([
             AdminFormElement::text('title', 'Title')->required(),
            AdminFormElement::ckeditor('main_content', 'Main Content'),
            AdminFormElement::textarea('meta_description', 'Meta Description')
        ]);
    });
});
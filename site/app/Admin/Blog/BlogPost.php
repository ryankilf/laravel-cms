<?php

use SleepingOwl\Admin\Model\ModelConfiguration;

AdminSection::registerModel(\App\Models\BlogPost::class, function (ModelConfiguration $model) {
    //$model->setTitle('Pages')->enableAccessCheck();

    // Display
    $model->onDisplay(function () {

        return AdminDisplay::table()
            ->setHtmlAttribute('class', 'table-primary')
            ->setColumns([
                AdminColumn::link('id')->setLabel('Id'),
                AdminColumn::email('title')->setLabel('Title'),
                AdminColumn::datetime('publish_date')->setLabel('Publish Date'),
                AdminColumn::custom()->setLabel('Publishable')->setCallback(function (\App\Models\BlogPost $model) {
                    return $model->publishable ? '<i class="fa fa-check"></i>' : '<i class="fa fa-minus"></i>';
                })->setWidth('50px')->setHtmlAttribute('class', 'text-center')->setOrderable(false),
            ])->paginate(20);
    });

    $model->onCreateAndEdit(function() {

        return AdminForm::form()->setItems([
            AdminFormElement::text('title', 'Title')->required(),
            AdminFormElement::time('publish_date', 'Publish Date (leave blank for current time)'),
            AdminFormElement::checkbox('publishable', 'Publishable'),
            AdminFormElement::select('category_id', 'Category')
            ->setModelForOptions(new \App\Models\BlogCategory())->required(),
            AdminFormElement::ckeditor('main_content', 'Main Content'),
            AdminFormElement::textarea('meta_description', 'Meta Description'),
            AdminFormElement::multiselect('blogTags', 'Tags')
            ->setModelForOptions(new \App\Models\BlogTag())
        ]);
    });
});
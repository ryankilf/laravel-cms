<?php
/**
 * Created by PhpStorm.
 * User: ryan
 * Date: 12/07/2016
 * Time: 12:59
 */

namespace App;


use App\Models\Log\LoggableModelTrait;
use App\Models\SiteSearch\SiteSearchItem;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Str;

trait MainPageContentItem
{
    use LoggableModelTrait;
    public $siteSearchTitleField = 'title';
    public $siteSearchContentFields = ['main_content', 'meta_description'];

    public function makeSlugOnSave(Model $model)
    {
        $this->setSlugFromField($model, 'slug', 'title');
    }

    public static function boot()
    {
        parent::boot();
        static::setupSiteSearchEvents();

        static::creating(function (Model $model) {
            /**
             * The functionality in here isn't called directly, so that you can take over the boot method in your own model
             * while still using this trait
             */
            $model->makeSlugOnSave($model);
        });

    }


    public static function setupSiteSearchEvents()
    {
        static::created(function (MainPageContentItemInterface $model) {
            $model->includeInSiteSearch($model);
        });

        static::updated(function (MainPageContentItemInterface $model) {
            $model->includeInSiteSearch($model);
        });

        static::deleting(function (MainPageContentItemInterface $model) {
            $model->removeFromSiteSearch($model);
        });

        if (method_exists(get_class(), 'restoring')) {
            static::restored(function (MainPageContentItemInterface $model) {
                $model->includeInSiteSearch($model);
            });
        }
    }

    /**
     * If, for example you have a blog post that isn't published yet, you can use this funciton for that
     *
     */
    public function shouldBeIncludedInSiteSearch():bool
    {
        return true;
    }

    protected function includeInSiteSearch(MainPageContentItemInterface $model)
    {
        if (!$model->shouldBeIncludedInSiteSearch()) {
            return $this->removeFromSiteSearch($model);
        }
        $siteSearchItem = SiteSearchItem::firstOrNew(
            ['page_id' => $model->getKey(),
                'class_name' => get_class($model)
            ]);

        $titleField = $model->siteSearchTitleField;
        $siteSearchItem->page_title = $model->{$titleField};

        foreach ($model->siteSearchContentFields as $contentField) {
            $siteSearchItem->page_content .= ' ' . strip_tags($model->{$contentField});
        }
        $siteSearchItem->full_path = $model->getFullPath();

        $siteSearchItem->save();
    }

    protected function removeFromSiteSearch(MainPageContentItemInterface $model)
    {
        try {
            $siteSearchItem = SiteSearchItem::where('page_id', $model->getKey())
                ->where('class_name', get_class($model));
            $siteSearchItem->delete();
        } catch (\Exception $e) {
        }
    }


    public function setSlugFromField(Model $model, string $slugFieldName, string $basedOnField)
    {

        $this->setSlugFromValue($model, $slugFieldName, $model->$basedOnField);
    }

    public function setSlugFromValue(Model $model, string $slugFieldName, string $slugValue)
    {
        $slugValue = Str::slug($slugValue);

        $i = '';
        do {
            $finalSlug = $slugValue . $i;
            $i--;
        } while ($model->where($slugFieldName, $finalSlug)->count() > 0);
        $model->$slugFieldName = $finalSlug;
    }


}
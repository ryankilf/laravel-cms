<?php
namespace App\Models;

use App\MainPageContentItem;
use App\MainPageContentItemInterface;
use App\Models\Log\LoggableModelInterface;
use App\Models\Log\LoggableModelTrait;
use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class BlogPost extends Model implements LoggableModelInterface, MainPageContentItemInterface {

    use MainPageContentItem;
    use SoftDeletes;
    protected $table = 'blog_posts';
    protected $dates = ['created_at', 'updated_at', 'deleted_at', 'publish_date'];

    public static function boot()
    {
        parent::boot();
        static::setupLoggingEvents();
        static::setupSiteSearchEvents();

        static::creating(function(Model $model)
        {

            /**
             * The functionality in here isn't called directly, so that you can take over the boot method in your own model
             * while still using this trait
             */
            $model->makeSlugOnSave($model);

            if($model->publish_date == null) {
                $model->publish_date = new Carbon();
            }
        });
    }


    public function blogTags()
    {
        return $this->belongsToMany(BlogTag::class);
    }

    public function blogCategory()
    {
        return $this->belongsTo(BlogCategory::class, 'category_id');
    }

    public function scopePublicPosts($query)
    {
        $now = new Carbon();
        $query->where('publish_date', '<=', $now);
        $query->where('publishable', '=', true);

    }

    /**
     * Any field names that you do not want to log, at all go in here
     * e.g. there's no point in logging the laravel updated_at timestamp, so it should go in here.
     *
     * @return array
     */
    public function getDoNotLogFieldNames(): array
    {
        return [];
    }

    public function getMaskedFieldNames(): array
    {
        return [];
    }

    function getFullPath(): String
    {
        return route('blog::item', [$this->blogCategory->slug, $this->slug]);
    }
}
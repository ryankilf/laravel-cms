<?php

namespace App\Models\SiteSearch;


use Illuminate\Database\Eloquent\Model;

class SiteSearchItem extends Model
{
    public $fillable = [ 'page_title', 'page_content', 'class_name', 'page_id', 'full_path'];
}
<?php
namespace App\Models;

use App\MainPageContentItem;
use App\MainPageContentItemInterface;
use App\Models\Log\LoggableModelInterface;
use App\Models\Log\LoggableModelTrait;
use Illuminate\Database\Eloquent\Model;

class BlogCategory extends Model implements LoggableModelInterface, MainPageContentItemInterface {

    use MainPageContentItem;

    protected $table = 'blog_categories';

    function getFullPath(): String
    {
        return route('blog::category', $this->slug);
    }
}
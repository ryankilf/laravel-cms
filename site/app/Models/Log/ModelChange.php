<?php
namespace App\Models\Log;


class ModelChange extends \Illuminate\Database\Eloquent\Model
{
    const CREATE = 'create';
    const UPDATE = 'update';
    const DELETE = 'delete';
    const RESTORE = 'restore';
    const MASKED_TEXT = '*MASKED*';
    const SESSION_CHANGE_LOGGING_ID = 'SESSION_MODEL_CHANGE_LOGGING_ID';
    const SYSTEM_NAME = 'SYSTEM';

    public static function getValidChangeValues() {
        return [
            self::CREATE,
            self::UPDATE,
            self::DELETE,
            self::RESTORE
        ];
    }

    public function getChangesAsArray(): array {
        return (array) json_decode($this->changes);
    }
}
<?php

namespace App\Models\Log;


interface LoggableModelInterface {

    /**
     * Any field names that you do not want to log, at all go in here
     * e.g. there's no point in logging the laravel updated_at timestamp, so it should go in here.
     * 
     * @return array
     */
    public function getDoNotLogFieldNames(): array;

    /**
     * If you want to log that a field has changed, but NOT what that field value is, it should go in here.
     * @return array
     */
    public function getMaskedFieldNames(): array;

    /**
     * A human readable, reasonable name for the model you wish to log
     * @return String
     */
    public static function getVisibleModelViewNameForLog(): String;

    /**
     * Label used for this model in the log, e.g. a Page Title, maybe a user's real name, that sort of thing
     * @return String
     */
    public function getLoggingLabel(): String;

    /**
     * This should normally return true, but can be set to false if you know you're going to make a change which
     * doesn't need to be logged (e.g. if a cron job is going to change this status multiple times in quick succession,
     * then it's just noise)
     * @return bool
     */
    public function getLogThisChange(): bool;

    
}
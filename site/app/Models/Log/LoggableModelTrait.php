<?php

namespace App\Models\Log;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\App;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Request;
use Illuminate\Support\Facades\Route;
use PhpParser\Node\Scalar\String_;
use Ramsey\Uuid\Uuid;


trait LoggableModelTrait
{

    public static function getVisibleModelViewNameForLog(): String
    {
        $className = get_called_class();
        $className = str_replace('\\', ' - ', $className);

        $re = '/(?<=[a-z])(?=[A-Z])/x';
        $a = preg_split($re, $className);
        $className = join($a, " " );

        return $className;
    }

    public function getLoggingLabel(): String
    {
        $label = $this->name ?? $this->label ?? $this->title;
        return $label;
    }

    public function getDisplayableModelClass(): String
    {
        return static::getVisibleModelViewNameForLog();
    }

    public static function setupLoggingEvents()
    {
        static::created(function(Model $model)
        {
            $model->logChange(ModelChange::CREATE, $model);
        });

        static::updating(function(Model $model)
        {
            $model->logChange(ModelChange::UPDATE, $model);
        });

        static::deleting(function(Model $model)
        {
            $model->logChange(ModelChange::DELETE, $model);
        });
        
        if(method_exists(get_class(), 'restoring')) {
            static::restoring(function(Model $model)
            {
                $model->logChange(ModelChange::RESTORE, $model);
            });
        }
    }

    public function getLogThisChange(): bool {
        $changedValues = $this->getDirty();
        $unloggedChanges = [$this->getUpdatedAtColumn()];
        foreach(array_keys($changedValues) as $changedKey ) {
            if(!in_array($changedKey, $unloggedChanges)) {
                return true;
            }
        }
        return false;
    }


    public static function boot()
    {
        parent::boot();

        static::setupLoggingEvents();
    }

    public function logChange(String $operation = '', Model $model)
    {
        if(!$this->getLogThisChange()) {
            return;
        }
        if (!in_array($operation, ModelChange::getValidChangeValues())) {
            throw new \Exception('Attempt to log model change that is not supported!');
        }

        $modelChange = new ModelChange();
        $modelChange->operation = $operation;
        $modelChange->request_id = self::getRequestId();
        $modelChange->session_id = self::getSessionId();
        $modelChange->model_id = $model->getKey();
        $modelChange->model_class = get_class($model);
        $modelChange->model_label = $model->getLoggingLabel();

        if (App::runningInConsole()) {
            $modelChange->user_name = ModelChange::SYSTEM_NAME;
        } else {
            $modelChange->route = Request::getFacadeRoot()->getPathInfo();
            $modelChange->method = Request::getFacadeRoot()->getMethod();
            $modelChange->client_ip = Request::ip();

            if(Auth::user()) {
                $user = Auth::user();
                $modelChange->user_id = $user->getKey();
                $modelChange->user_name = $user->name;
            }

        }

        if ($operation == ModelChange::CREATE) {
            $changes = $model->getAttributes();
        } else {
            $changes = $model->getDirty();
        }

        $noNotLogFieldNames = $model->getDoNotLogFieldNames();
        $maskedFieldNames = $model->getMaskedFieldNames();

        foreach ($changes as $key => $change) {
            if (in_array($key, $noNotLogFieldNames)) {
                unset($changes[$key]);
            } elseif (in_array($key, $maskedFieldNames)) {
                $changes[$key] = ModelChange::MASKED_TEXT;
            }
        }

        //$modelChange->changes = gzencode(json_encode($changes), 9);
        $modelChange->changes = json_encode($changes);

        $modelChange->save();
    }

    private static function getRequestId()
    {
        static $requestId;
        if (is_null($requestId)) {
            $requestId = Uuid::uuid4()->toString();
        }
        return $requestId;
    }

    private static function getSessionId()
    {
        static $sessionId;

        if (App::runningInConsole()) {
            return null;
        }

        if (is_null($sessionId)) {

            $sessionId = session()->get(ModelChange::SESSION_CHANGE_LOGGING_ID, Uuid::uuid4()->toString());

            session()->put(ModelChange::SESSION_CHANGE_LOGGING_ID, $sessionId);
        }

        return $sessionId;
    }

    /**
     * Any field names that you do not want to log, at all go in here
     * e.g. there's no point in logging the laravel updated_at timestamp, so it should go in here.
     *
     * @return array
     */
    public function getDoNotLogFieldNames(): array
    {
        return ['updated_at', 'created_at', 'deleted_at'];
    }

    /**
     * If you want to log that a field has changed, but NOT what that field value is, it should go in here.
     * @return array
     */
    public function getMaskedFieldNames(): array
    {
        return ['password'];
    }

}
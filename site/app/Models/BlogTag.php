<?php
namespace App\Models;


use App\MainPageContentItem;
use App\MainPageContentItemInterface;
use App\Models\Log\LoggableModelInterface;
use Illuminate\Database\Eloquent\Model;

class BlogTag extends Model implements LoggableModelInterface, MainPageContentItemInterface {
    
    use MainPageContentItem;

    protected $table = 'blog_tags';

    public function blogPost()
    {
        return $this->belongsToMany('App\Models\BlogPost');
    }


    function getFullPath(): String
    {
        return route('blog::tag', $this->slug);
    }
}
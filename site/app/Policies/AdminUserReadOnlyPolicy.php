<?php

namespace App\Policies;

use App\Models\Log\ModelChange;
use App\User;
use App\Role;
use Illuminate\Auth\Access\HandlesAuthorization;

class AdminUserReadOnlyPolicy
{

    use HandlesAuthorization;

    /**
     * @param User   $user
     * @param string $ability
     *
     * @param Role   $item
     *
     * @return bool
     */
    public function before(User $user, $ability, ModelChange $item)
    {
        if ($user->isSuperAdmin()) {
            if ($ability != 'display') {
                return false;
            }
            return true;
        }

        return false;
    }

    /**
     * @param User $user
     * @param Role $item
     *
     * @return bool
     */
    public function display(User $user, ModelChange $item)
    {
        return $user->isSuperAdmin();
    }

    /**
     * @param User $user
     * @param Role $item
     *
     * @return bool
     */
    public function edit(User $user, ModelChange $item)
    {
        return false;
    }

    /**
     * @param User $user
     * @param Role $item
     *
     * @return bool
     */
    public function delete(User $user, ModelChange $item)
    {
        return false;
    }
}
